__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/game-of-rotation

input_len = input()

belt = map(int, raw_input().split(" "))

sum_of_belt = sum(belt)

max_pmean = reduce(lambda x, y: x + y * belt[y - 1], xrange(0, input_len + 1))
temp_pmean = max_pmean

for i in xrange(len(belt) - 1):
    temp_pmean = temp_pmean - sum_of_belt + (belt[i] * input_len)
    if temp_pmean > max_pmean:
        max_pmean = temp_pmean

print max_pmean