__author__ = 'joychugh'
# https://www.hackerrank.com/challenges/game-of-thrones

string = raw_input()
charmap = {}

for character in string:
    if character in charmap:
        charmap[character] = charmap[character] ^ True
    else:
        charmap[character] = True

def isPalindrome(charmap):
    odd_seen = 0;
    for value in charmap.values():
        if odd_seen > 1:
            return False
        if value:
            odd_seen += 1
    return True

if isPalindrome(charmap):
    print("YES")
else:
    print("NO")