__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/sherlock-and-watson

N, K, Q = map(int, raw_input().split())
ARRAY = map(int, raw_input().split())


def find_element_at(location):
    tk = K
    while tk > 0:
        location -= 1
        if location < 0:
            location = N - 1
        tk -= 1
    return location

for query in range(Q):
    print ARRAY[find_element_at(input())]