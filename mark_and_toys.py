__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/mark-and-toys

# Enter your code here. Read input from STDIN. Print output to STDOUT

def max_toys(prices, money):
    prices.sort()
    answer = 0
    for price in prices:
        if money - price < 0:
            break
        money -= price
        answer += 1
    return answer


if __name__ == '__main__':
    n, k = map(int, raw_input().split())
    prices = map(int, raw_input().split())
    print max_toys(prices, k)
