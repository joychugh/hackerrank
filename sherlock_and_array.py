__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/sherlock-and-array

def isValid(target_array):
    arr_size = len(target_array)
    if arr_size == 2:
        return "NO"
    if arr_size == 1:
        return "YES"
    lindex = 0
    rindex = arr_size - 1
    arr_size -= 2
    lsum = target_array[lindex]
    rsum = target_array[rindex]
    while arr_size > 1:
        if lsum > rsum:
            rindex -= 1
            arr_size -= 1
            rsum += target_array[rindex]
        elif lsum < rsum:
            lindex += 1
            arr_size -= 1
            lsum += target_array[lindex]
        else:
            rindex -= 1
            lindex += 1
            arr_size -= 2
            rsum += target_array[rindex]
            lsum += target_array[lindex]
    if arr_size == 1 and lsum == rsum:
        return "YES"
    return "NO"

test_cases = input()

for test_case in xrange(test_cases):
    arr_size = input()
    arr = map(int, raw_input().split(" "))
    print isValid(arr)
