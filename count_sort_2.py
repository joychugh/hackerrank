__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/countingsort2

arr_len = input()
arr = [0] * 100
input_arr = map(int, raw_input().split(" "))
for item in input_arr:
    arr[item] += 1

sorted_arr = []

for i in xrange(len(arr)):
    if arr[i] > 0:
        sorted_arr.extend([str(i)] * arr[i])

print " ".join(sorted_arr)
