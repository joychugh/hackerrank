__author__ = 'joychugh'
# https://www.hackerrank.com/challenges/find-digits

def get_div_set(num):
    div_set = set()
    for x in xrange(1, 10):
        if not num % x:
            div_set.add(x)
    return div_set


testcase = int(raw_input())

for test in xrange(testcase):
    num = int(raw_input())
    divset = get_div_set(num)
    count = 0
    while num:
        num, digit = divmod(num, 10)
        if digit != 0 and digit in divset:
            count += 1
    print count