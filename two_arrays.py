__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/two-arrays

test_cases = input()

for test_case in range(test_cases):
    arr_len, min_sum = raw_input().split(" ")
    arr_a = map(int, raw_input().split(" "))
    arr_b = map(int, raw_input().split(" "))
    arr_a.sort()
    arr_b.sort(None, None, True)
    valid = True
    for index in xrange(int(arr_len)):
        if arr_a[index] + arr_b[index] < int(min_sum):
            valid = False
            break
    print "YES" if valid else "NO"
