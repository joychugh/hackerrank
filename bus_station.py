__author__ = 'joychugh'
import math
# https://www.hackerrank.com/challenges/bus-station

number_of_groups = input()
groups = map(int, raw_input().split(" "))


total_sum = sum(groups)
answer = []


def check_bus(factor):
    bus_sum = 0
    inv = False
    for group in groups:
        bus_sum += group
        if bus_sum == factor:
            bus_sum = 0
        elif bus_sum > factor:
            inv = True
            break
    if not inv:
        answer.append(factor)


def check_factors(possible_factor, total):
    """
    Took help from Editorial to solve the time out on large numbers
    The xrange to math.sqrt was taken from editorial. Earlier 
    I was using it till total_sum/2.
    """
    if total % possible_factor == 0:
        check_bus(possible_factor)
        if total / possible_factor != possible_factor:
            check_bus(total / possible_factor)


map(lambda pf: check_factors(pf, total_sum), xrange(1, int(math.sqrt(total_sum)) + 1))
answer.sort()
print " ".join(map(str, answer))