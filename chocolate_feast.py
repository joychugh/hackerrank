__author__ = 'joychugh'
# https://www.hackerrank.com/challenges/chocolate-feast

# Enter your code here. Read input from STDIN. Print output to STDOUT
T = int(raw_input())
for i in range (0,T):
    N,C,M = [int(x) for x in raw_input().split(' ')]
    initial_chocolate = N/C
    covers = initial_chocolate
    additional_chocolate = covers/M
    # We consumed the covers in the previous step so calculate them again
    covers = covers % M + additional_chocolate
    final_chocolates = additional_chocolate + initial_chocolate
    while additional_chocolate:
        additional_chocolate = covers/M
        covers = covers % M + additional_chocolate
        final_chocolates += additional_chocolate
    # write code to compute answer
    print final_chocolates