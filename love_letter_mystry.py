__author__ = 'joychugh'
# https://www.hackerrank.com/challenges/the-love-letter-mystery

test_cases = int(raw_input())

for test in range(test_cases):
    word = raw_input()
    diff = 0
    for i in xrange(len(word) / 2):
        diff += abs(ord(word[i]) - ord(word[len(word) - 1 - i]))
    print diff