__author__ = 'joychugh'

# not hackerrank

nums = int(raw_input())

def getFibo(count):
    fibo = [n * 0 - 1 for n in xrange(count + 1)]
    fibo[0] = 0
    fibo[1] = 1
    for i in xrange(2, count + 1):
        fibo[i] = fibo[i - 1] + fibo[i - 2]