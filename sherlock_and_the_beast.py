__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/sherlock-and-the-beast


def find_largest_decent(req_length):
    quotient, remainder = divmod(req_length, 3)
    if not remainder:
        return "5" * 3 * quotient
    else:
        templen = req_length - remainder
        while templen:
            templen -= 3
            if not (remainder + 3) % 5:
                return "5" * templen + "3" * (remainder + 3)
            else:
                remainder += 3
    return "-1"


test_cases = int(raw_input())

for x in xrange(test_cases):
    print find_largest_decent(int(raw_input()))