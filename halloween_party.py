__author__ = 'joychugh'
# https://www.hackerrank.com/challenges/halloween-party


test_cases = int(raw_input())

for test_case in xrange(test_cases):
    case = int(raw_input())
    case, base = divmod(case, 2)
    print case * (case + base)