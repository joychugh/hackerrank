__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/sherlock-and-pairs

test_cases = input()
counter = {}


def count(var):
    global counter
    if var in counter:
        counter[var] += 1
    else:
        counter[var] = 1

for case in xrange(test_cases):
    global counter
    counter = {}
    input_len = input()
    map(count, raw_input().split())
    answer = 0
    for val in counter.values():
        answer += val * (val - 1)
    print answer