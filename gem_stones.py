__author__ = 'joychugh'
# https://www.hackerrank.com/challenges/gem-stones

rocks_count = int(raw_input())
rocks = []
for count in xrange(rocks_count):
    rocks.append(raw_input())

# Convert the elements to a set to remove duplicates.
set_list = []
for rock in rocks:
    set_list.append(set(rock))

#Get the first element
result_set = set_list[0]

# Run on rest of the elements
for element_set in set_list[1:]:
    result_set = result_set.intersection(element_set)

print len(result_set)