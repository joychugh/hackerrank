__author__ = 'joychugh'

#https://www.hackerrank.com/challenges/sherlock-and-minimax

item_count = input()
items = map(int, raw_input().split(" "))
P, Q = map(int, raw_input().split(" "))
items.sort()


def find_closest_item(closest_to, item_list):
    """
    :rtype : int
    """
    if closest_to < P or closest_to > Q:
        return -1
    if item_list[0] > closest_to:
        return abs(item_list[0] - closest_to)
    if item_list[-1] < closest_to:
        return abs((item_list[-1] - closest_to))
    if len(item_list) == 1:
        return abs(item_list[0] - closest_to)
    if len(item_list) == 2:
        return abs(item_list[0] - closest_to) \
            if abs(item_list[0] - closest_to) < abs(item_list[1] - closest_to) \
            else abs(item_list[1] - closest_to)
    mid = len(item_list) / 2
    mid_val = abs(item_list[mid] - closest_to)
    mid_prev_val = abs(item_list[mid - 1] - closest_to)
    mid_next_val = abs(item_list[mid + 1] - closest_to)
    if not mid_val or not mid_next_val or not mid_prev_val:
        return 0
    if mid_prev_val == mid_next_val:
        return mid_val
    if item_list[mid] < closest_to:
        if closest_to < item_list[mid + 1]:
            return min(mid_val, mid_next_val)
        if closest_to > item_list[mid + 1]:
            return find_closest_item(closest_to, item_list[mid + 1:])
    if item_list[mid] > closest_to:
        if closest_to > item_list[mid - 1]:
            return min(mid_val, mid_prev_val)
        if closest_to < item_list[mid - 1]:
            return find_closest_item(closest_to, item_list[:mid])


min_max_item = P
max_min_item_val = find_closest_item(P, items)
Q_min_item_val = find_closest_item(Q, items)
min_max_item = Q if Q_min_item_val > max_min_item_val else P
for index in xrange(item_count):
    for sub_index in xrange(index + 1, item_count):
        mid = (items[index] + items[sub_index]) / 2
        min_item_val = find_closest_item(mid, range(items[index], items[sub_index] + 1))
        if min_item_val > max_min_item_val:
            min_max_item = mid
            max_min_item_val = min_item_val
        elif min_item_val == max_min_item_val and mid < min_max_item:
            min_max_item = mid

print min_max_item