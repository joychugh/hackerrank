__author__ = 'joychugh'


def max_xor(l, r):
    p = l ^ r
    print bin(p)
    ret = 1
    while p:
        ret <<= 1
        print "ret: {0}".format(bin(ret))
        p >>= 1
        print "xor: {0}".format(bin(p))
    return ret - 1

print max_xor(0, 16)