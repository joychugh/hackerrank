__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/angry-children

n = input()
k = input()
candies = [input() for _ in range(0, n)]
candies.sort()

s = 0
e = s + k - 1

min_diff = candies[-1]

while e < len(candies):
    t_diff = candies[e] - candies[s]
    if t_diff < min_diff:
        min_diff = t_diff
    s += 1
    e = s + k - 1

print min_diff