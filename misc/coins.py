__author__ = 'joychugh'

denominations = map(int, raw_input("Enter Denominations in Comma separated format eg, 1,2,3: ").split(","))
amount = input("Enter amount")

stuff = [0] * (amount + 1)
stuff[0] = 1

for denomination in denominations:
    for i in range(denomination, amount + 1):
        cat = i - denomination
        stuff[i] += stuff[cat]

print stuff