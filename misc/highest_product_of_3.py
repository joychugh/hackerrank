__author__ = 'joychugh'

# https://www.interviewcake.com/question/highest-product-of-3


def highest_product_of_3(input_array):
    # If there are just 3 elements
    if len(input_array) == 3:
        return reduce(lambda x, y: x * y, input_array)

    highest = input_array[0]
    lowest = input_array[0]
    highest_of_3 = reduce(lambda x, y: x * y, input_array[:3])
    highest_of_2 = input_array[0] * input_array[1]
    lowest_of_2 = input_array[0] * input_array[1]
    for item in input_array[2:]:
        highest_of_3 = max(highest_of_3, highest_of_2 * item, lowest_of_2 * item)
        highest_of_2 = max(highest_of_2, highest * item, lowest * item)
        lowest_of_2 = min(lowest_of_2, highest * item, lowest * item)
        highest = max(highest, item)
        lowest = min(lowest, item)
    return highest_of_3

