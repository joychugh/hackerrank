#
# Given a number M (N-digit integer) and K-swap operations(a swap
# operation can swap 2 digits), devise an algorithm to get the maximum possible integer?
# Examples:
# M = 132 K = 1 output = 312
# M = 132 K = 2 output = 321
# M = 7899 k = 2 output = 9987



def dfs():
    mat = {
    'a': ['b', 'c', 'd'],
    'b': ['d'],
    'c': ['e', 'g', 'd', 'i'],
    'd': ['f', 'g'],
    'e': ['g'],
    'f': ['g'],
    'g': [],
    'i': ['g']}

    visited = ['a']
    parent = {'a': None}
    while visited:
        item = visited.pop()
        for child in mat[item]:
            if child not in parent:
                parent[child] = item
                visited.append(child)
            print visited
    print parent

def get_one_digit_swap(num_input):
    ret = []
    for i in range(len(num_input)):
        temp = num_input[:]
        for j in range(i+1, len(num_input)):
            temp[i], temp[j] = temp[j], temp[i]
            ret.append(temp)
            temp = num_input[:]
    return ret


def generate_graph():
    number = 123
    k = 2
    graph = {}
    visited = [numlist]
    numlist = listize_number(number)
    largest = listize_number(number)
    largest.sort(reverse=True)
    graph[numlist] = get
    for i in range(k):
        while visited:
            to_process = visited.pop()
            graph[to_process] = get_one_digit_swap(to_process)
            visited.append(graph[to_process])

    print get_one_digit_swap(numlist)





def listize_number(number):
    numlist = []
    while number:
        number, digit = divmod(number, 10)
        numlist.append(digit)
    numlist.reverse()
    return numlist

generate_graph()