
# Design a program that reads a matrix of "n" rows for "m" columns and determine whether prime or not.
# A matrix A is prime if the sum of the elements of any of its rows is equal to the sum of the elements of any column.

example_prime_matrix = [[1, 1, 1, 1],
                        [2, 1, 1, 1],
                        [1, 1, 1, 1],
                        [1, 1, 1, 1]]


example_non_prime_matrix = [[1, 1, 1, 1],
                            [1, 1, 1, 1],
                            [1, 1, 1, 1]]


def is_prime_matrix(matrix):
    """
    given a matrix it will check if its prime or not
    Will return True or False
    """

    # Calculate the sum of rows and store it in a set. We just need to have Unique values to check against.
    row_sums = set()
    for row in matrix:
        row_sums.add(sum(row))

    #Calculate the sums of the columns.
    column_sums = [0 for x in range(len(matrix[0]))]
    for row in matrix:
        column_sums = [column_sums[i] + row[i] for i in range(len(row))]

    # For each sum of column check if equal value exists in the sum of rows.
    # if yes return true
    for element in column_sums:
        if element in row_sums:
            return True
    # If no similar value found, return false
    return False

print is_prime_matrix(example_prime_matrix)
print is_prime_matrix(example_non_prime_matrix)
