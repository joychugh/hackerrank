__author__ = 'joychugh'

# https://www.interviewcake.com/question/merging-ranges

def condense_time_range(input_time_ranges):
    # Sort the tuple
    input_time_ranges.sort(key=lambda tup: tup[0])
    output_tuple_list = []
    for item in input_time_ranges:
        if not output_tuple_list:
            output_tuple_list.append(item)
        if output_tuple_list[-1][1] < item[0]:
            output_tuple_list.append(item)
        elif item[0] <= output_tuple_list[-1][1]:
            # extend the range
            output_tuple_list[-1][1] = max(item[1], output_tuple_list[-1][1])
    return output_tuple_list

print condense_time_range([[0, 1], [3, 5], [4, 8], [10, 12], [9, 10]])
print condense_time_range([[1, 2], [2, 3]])
print condense_time_range([[1, 5], [2, 3]])
print condense_time_range([[1, 10], [2, 6], [3, 5], [7,9]])