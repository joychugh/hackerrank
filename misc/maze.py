__author__ = 'joychugh'


end_i = 10
end_j = 10


def is_valid(i):
    if 0 <= i[0] <= end_i - 1 and 0 <= i[1] <= end_j - 1:
        return True
    else:
        return False


def get_next(i):
    return [[i[0] + 1, i[1]], [i[0], i[1]+1], [i[0]-1, i[1]], [i[0], i[1]-1]]


def bfs(start):
    parent = {str(start): None}
    level = {str(start): 0}
    level_count = 1
    frontier = [start]
    while frontier:
        traversal_list = []
        for adj_vertex in frontier:
            tl = [x for x in get_next(adj_vertex) if is_valid(x)]
            for adj_node in tl:
                if str(adj_node) not in parent:
                    parent[str(adj_node)] = adj_vertex
                    level[str(adj_node)] = level_count
                    level_count += 1
                    traversal_list.append(adj_node)
            frontier = traversal_list
            level_count += 1
    x = parent[str([9, 9])]
    print x
    while x:
        x = parent[str(x)]
        print x

bfs([0,0])