__author__ = 'joychugh'

import itertools

def calculate_max(expenses, max_limit):
    maxexp = 0
    max_answer = []
    for i in range(len(expenses)):
        supposed_max_entries = itertools.combinations(expenses, i)
        for supposed_max_entry in supposed_max_entries:
            supposed_max = sum(supposed_max_entry)
            if maxexp < supposed_max < max_limit:
                maxexp = supposed_max
                max_answer = supposed_max_entry
    return max_answer, maxexp


def calculate_max_2(x, max_limit):
    max_matrix = [[0 for i in range(max_limit + 1)] for y in range(len(x) + 1)]
    for item in range(1, len(x) + 1):
        for weight in range(1, max_limit + 1):
            if weight >= x[item-1]:
                max_matrix[item][weight] = max(max_matrix[item-1][weight-x[item-1]] + x[item-1], max_matrix[item-1][weight])
            else:
                max_matrix[item][weight] = max_matrix[item-1][weight]
    for i in range(len(max_matrix)):
        print " ".join(map(str, max_matrix[i]))
        print "\n"





print calculate_max([19.16, 9.45, 17, 35, 8.44, 12.8, 9.63, 75], 150)
calculate_max_2([2, 3, 5, 1], 15)
