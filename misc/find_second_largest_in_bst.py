__author__ = 'joychugh'


# https://www.interviewcake.com/question/second-largest-item-in-bst

def find_largest(tree_head):
    if not tree_head.right:
        return tree_head
    else:
        return find_largest(tree_head.right)

def find_2nd_largest(tree_head):
    if tree_head.right and tree_head.right.right:
        return find_2nd_largest(tree_head.right)
    elif tree_head.right and not tree_head.right.right:
        return tree_head
    elif tree_head.right and tree_head.right.left and not tree_head.right.right:
        return find_largest(tree_head.right.left)

def find_second_largest(tree_head):
    # If tree_head is none or if its the only node.
    if not tree_head or not (tree_head.left or tree_head.right):
        return None

    # IF the tree root node is on the top. (is the largest, just find the largest node on the left subtree)
    if not tree_head.right:
        return find_largest(tree_head.left)
    if tree_head.right:
        return find_2nd_largest(tree_head)


