__author__ = 'joychugh'

def quickSort(ar):
    if len(ar) == 1:
        return ar
    if len(ar) > 1:
        pivot = ar[0]
        left = [x for x in ar if x < pivot]
        right = [x for x in ar if x > pivot]
        ret = []
        qsl = quickSort(left)
        qsr = quickSort(right)
        ret.extend(qsl)
        ret.append(pivot)
        ret.extend(qsr)
        print ret
        return ret

m = input()
ar = [int(i) for i in raw_input().strip().split()]
quickSort(ar)
