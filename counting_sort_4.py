__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/countingsort4

items = input()

sort_helper = [[] for y in xrange(100)]

for item in xrange(items/2):
    item_no, item_val = raw_input().split(" ")
    sort_helper[int(item_no)].append("-")

for item in xrange(items/2):
    item_no, item_val = raw_input().split(" ")
    sort_helper[int(item_no)].append(item_val)

print " ".join([" ".join(elements) for elements in sort_helper])