__author__ = 'joychugh'
# https://www.hackerrank.com/challenges/manasa-and-stones
# I could not solve the problem as the editorial expected in O(n) time but these are my 2 solutions.
# One uses binary tree kind of approach, other uses similar approach just uses stacks.(like Breadth First Search)

def get_last_number(output_set, inputs, inputl, set_max_size, a, b):
    if inputl == set_max_size:
        output_set.add(inputs)
    else:
        get_last_number(output_set, inputs + a, inputl + 1, set_max_size, a, b)
        get_last_number(output_set, inputs + b, inputl + 1, set_max_size, a, b)
    return


def get_children(n, height, max_height, a, b):
    if height > max_height:
        return []
    return [n + a, n + b]


def bfs(start, a, b, max_size):
    output_set = list()
    start_list = list()
    start_list.append(start)
    slen = 1
    frontier = []
    while start_list:
        if slen >= max_size:
            return frontier
        frontier = []
        for element in start_list:
            for child in get_children(element, slen + 1, max_size, a, b):
                frontier.append(child)
        slen += 1
        start_list = frontier

test_cases = int(raw_input())

for case in xrange(test_cases):
    stones = int(raw_input())
    a = int(raw_input())
    b = int(raw_input())
    outputs = set()
    output_list = bfs(0, a, b, stones)
    output_list.sort()
    outputs = set(output_list)
    #get_last_number(outputs, 0, 1, stones, a, b)
    print " ".join(str(x) for x in outputs)