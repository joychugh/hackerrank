__author__ = 'joychugh'

# https://www.hackerrank.com/challenges/cavity-map

map_len = input()

cavity_map = []
result_map = []

for x in xrange(map_len):
    cavity_map.append([int(digit) for digit in raw_input()])
    result_map.append(cavity_map[x])


def is_cavity(matrix, i, j):
    cur = matrix[i][j]
    if matrix[i - 1][j] < cur \
            and matrix[i + 1][j] < cur \
            and matrix[i][j - 1] < cur \
            and matrix[i][j + 1] < cur:
        return True
    return False

for row in xrange(1, map_len - 1):
    for column in xrange(1, map_len - 1):
        if is_cavity(cavity_map, row, column):
            result_map[row][column] = 'X'

for row in result_map:
    print "".join(map(str, row))




